# TV-webcon

## 概要

Raspberry PI を HDMI で TV と繋ぐことでTVの操作ができるAPIサーバーです。  
現在は電源のON/OFFのみ対応しています。

## 始め方

1. docker-compose で始める

```sh
$ git clone https://gitlab.com/harusame0616/tv-webcon
$ cd tv-webcon
$ docker-compose -f docker-compose.yml -f docker-compose.production.yml build
$ docker-compose up

# 電源ONテスト
$ curl -X POST -H "Content-Type: application/json" -d '{"power": "on"}' http://localhost:8080/power
```

## API

### 電源の操作 [POST : /power]
#### パラメーター
- power[string, 必須]: on / off - 変更する電源の状態
