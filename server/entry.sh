#!/bin/bash

USER_ID=${LOCAL_UID:1000}

useradd -omu ${LOCAL_UID} app
usermod -aG video app
chown app:app ./

exec /usr/sbin/gosu app "$@"
