import { TvController } from './tv-controller';

class TvApplication {
  constructor(private tvController: TvController) { }
  async powerOn() {
    await this.tvController.powerOn();
  }

  async powerOff() {
    await this.tvController.powerOff();
  }
}

export default TvApplication;
