export interface TvController {
  powerOn(): Promise<void>;
  powerOff(): Promise<void>;
}
