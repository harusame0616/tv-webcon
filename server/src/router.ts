import { Router } from 'express';
import { CecTvController } from './controllers/cec-tv-controller';
import TvApplication from './usecases/tv-application';


const tvApplication = new TvApplication(new CecTvController());
const router = Router();
router.post('/power', async (req, res) => {
  const { power } = req.body;

  if (power === 'on') {
    await tvApplication.powerOn();
  } else if (power === 'off') {
    await tvApplication.powerOff();
  } else {
    return res.send({ status: 400, error: 'power must be "on" or "off"' })
  }

  return res.send();
});

export default router;
