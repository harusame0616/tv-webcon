import { TvController } from '../usecases/tv-controller';
import { promisify } from 'util'
import { exec } from 'child_process'
const asyncExec = promisify(exec);

export class CecTvController implements TvController {
  async powerOn(): Promise<void> {
    await asyncExec('echo "on 0" | cec-client -s')
  }

  async powerOff(): Promise<void> {
    await asyncExec('echo "standby 0" | cec-client -s')
  }
}
