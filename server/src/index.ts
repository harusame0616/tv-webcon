import express from 'express';
import router from './router';

const app: express.Express = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.header('Access-Control-Allow-Origin', process.env.CROS_ORIGIN ?? '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST');
    res.header('Access-Control-Allow-Headers', '');
    res.header('Content-Type', 'application/json');
    next();
  }
);

app.listen(3000, () => {
  console.info('Start on port 3000.');
});

app.use('/', router);

app.use(
  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    return res.status(400).send({ status: 400, error: 'not found' });
  }
);
